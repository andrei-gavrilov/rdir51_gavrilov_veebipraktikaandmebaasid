<?php
require_once "autoloader.php";

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Movies, veebipraktika - andmebaasid</title>
		<link rel="stylesheet" href="bootstrap/css/bootstrap.css">
        <script src="bootstrap/js/bootstrap.js" ></script>
		<link rel="stylesheet" href="css/myStyle.css">
    </head>
    <body>
	<div class="container">
		<div class="row">
			<div class="col-12">
				<nav class="navbar navbar-toggleable-md navbar-light bg-faded">
      				<h1 class="navbar-brand mb-0">Veebipraktika - andmebaasid</h1>
					<div id="navbarNav">
    					<ul class="navbar-nav">
      						<li class="nav-item"><a class="nav-link" href="index.php">Task</a></li>
      						<li class="nav-item "><a class="nav-link" href="categories.php">Categories <span class="sr-only">(current)</span></a></li>
							<li class="nav-item"><a class="nav-link" href="actors.php">Actors</a></li>
    					</ul>
  					</div>
    			</nav>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<?php 
				 $db=new PDOService();


				if(isset($_GET['categoryid'])){ ?>
					<h1><?php echo $db->getCategoryByID($_GET['categoryid'])->name ?> films</h1>
					<?php 
					$films=$db->getFilmsByCategory($_GET['categoryid']);
					if(empty($films)){ ?>
						<h2>No films in this category!</h2>
						<h3><a href="categories.php">Return</a></h3>
					<?php } else { ?>
						<table class="table">
               				<thead class="thead-dark">
                    			<tr>
                        			<th scope="col">Title</th>
                        			<th scope="col">Description</th>
                        			<th scope="col">Year</th>
									<th scope="col">Length</th>
                    			</tr>
                			</thead>
                			<tbody>
                    			<?php 
                    			foreach ($films as $film) { ?>
                        		<tr>
                            		<td><?php echo $film->title; ?></td>
                            		<td><?php echo $film->description; ?></td>
                            		<td><?php echo $film->releaseYear; ?></td>
									<td><?php echo $film->length; ?></td>
                        		</tr>
                    			<?php } ?>

                			</tbody>
            			</table>
						<h3><a href="categories.php">Return</a></h3>
					<?php } } ?>  
				<?php
				if(isset($_GET['actorid'])){ 
					$actor=$db->getActorById($_GET['actorid']);
					?>
					<h1><?php echo $actor->firstname." ".$actor->lastname ?></h1>
					<?php 
					
					$films=$db->getFilmsByActor($_GET['actorid']);
					if(empty($films)){ ?>
						<h2>No films for this actor!</h2>
						<h3><a href="actors.php">Return</a></h3>
					<?php } else { ?>
						<table class="table">
               				<thead class="thead-dark">
                    			<tr>
                        			<th scope="col">Title</th>
                        			<th scope="col">Description</th>
                        			<th scope="col">Year</th>
									<th scope="col">Length</th>
                    			</tr>
                			</thead>
                			<tbody>
                    			<?php 
                    			foreach ($films as $film) { ?>
                        		<tr>
                            		<td><?php echo $film->title; ?></td>
                            		<td><?php echo $film->description; ?></td>
                            		<td><?php echo $film->releaseYear; ?></td>
									<td><?php echo $film->length; ?></td>
                        		</tr>
                    			<?php } ?>

                			</tbody>
            			</table>
						<h3><a href="actors.php">Return</a></h3>
						<?php } ?> 
				<?php } ?> 
			</div>
		</div>
	</div>
	<footer class="footer">
      		<div class="container">
       		<p class="text-muted">© 2017 Andrei Gavrilov, RDIR51</p>
     		</div>
    	</footer>
    </body>
</html>
