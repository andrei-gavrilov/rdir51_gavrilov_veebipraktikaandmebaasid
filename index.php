<?php
require_once "autoloader.php";

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Task, veebipraktika - andmebaasid</title>
		<link rel="stylesheet" href="bootstrap/css/bootstrap.css">
		<link rel="stylesheet" href="css/myStyle.css">
        <script src="bootstrap/js/bootstrap.js" ></script>
		
    </head>
    <body>
	<div class="container">
		<div class="row">
			<div class="col-12">
				<nav class="navbar navbar-toggleable-md navbar-light bg-faded">
      				<h1 class="navbar-brand mb-0">Veebipraktika - andmebaasid</h1>
					<div id="navbarNav">
    					<ul class="navbar-nav">
      						<li class="nav-item active"><a class="nav-link" href="index.php">Task <span class="sr-only">(current)</span></a></li>
      						<li class="nav-item"><a class="nav-link" href="categories.php">Categories</a></li>
							<li class="nav-item"><a class="nav-link" href="actors.php">Actors</a></li>
    					</ul>
  					</div>
    			</nav>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<h1>Ülesanne. Maailma filmid (30 p.)</h1>
				<ol class="list-group">
					<li class="list-group-item">	Kommenteerige programmi kood Example. MoviesDB (15 p.)</li>
					<li class="list-group-item">	Koostage menüü - (Category) (5 p.) - lisage getAllCategories funktsioon klassi.</li>
					<li class="list-group-item">	Kuvage filmide loetelu valitud kategooria järgi (5 p.)</li>
					<li class="list-group-item">	Looge leht Näitlejad (andmed sorteeritud perekonnanime järgi kasvavas järjekorras ). Kuvage filmide loetelu  valitud näitleja järgi (5 p.)</li>
					<li class="list-group-item">	Kasutage Front-End CSS Framework (näiteks, Bootstrap,...)</li>
				</ol>
			</div>
		</div>
	</div>
	<footer class="footer">
      		<div class="container">
       		<p class="text-muted">© 2017 Andrei Gavrilov, RDIR51</p>
     		</div>
    	</footer>
    </body>
</html>
