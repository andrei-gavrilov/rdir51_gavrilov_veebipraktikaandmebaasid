<?php
require_once "autoloader.php";

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Categories, veebipraktika - andmebaasid</title>
		<link rel="stylesheet" href="bootstrap/css/bootstrap.css">
		<link rel="stylesheet" href="css/myStyle.css">
        <script src="bootstrap/js/bootstrap.js" ></script>
    </head>
    <body>
	<div class="container">
		<div class="row">
			<div class="col-12">
				<nav class="navbar navbar-toggleable-md navbar-light bg-faded">
      				<h1 class="navbar-brand mb-0">Veebipraktika - andmebaasid</h1>
					<div id="navbarNav">
    					<ul class="navbar-nav">
      						<li class="nav-item"><a class="nav-link" href="index.php">Task</a></li>
      						<li class="nav-item active"><a class="nav-link" href="categories.php">Categories <span class="sr-only">(current)</span></a></li>
							<li class="nav-item"><a class="nav-link" href="actors.php">Actors</a></li>
    					</ul>
  					</div>
    			</nav>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<h1>Film categories</h1>
				
				
                <?php
                //Используется PDO для доступа к базе данных.
			    $db=new PDOService();?>

                <ul class="list-group">
				<?php foreach ($db->getAllCategories() as $category) { ?>
                    <a href="movies.php?categoryid=<?php echo $category->id;  ?>" class="list-group-item list-group-item-action">
                        <?php echo $category->name ?>
                    </a>
            <?php } ?>
            </ul>
			
			</div>
		</div>
	</div>
	<footer class="footer">
      		<div class="container">
       		<p class="text-muted">© 2017 Andrei Gavrilov, RDIR51</p>
     		</div>
    	</footer>
    </body>
</html>
