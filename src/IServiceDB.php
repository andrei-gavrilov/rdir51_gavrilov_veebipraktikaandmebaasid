<?php

//Интерфейс, осуществляемый классами MySQLiService.php и PDOService.php
interface IServiceDB
{
    public function connect();
    public function getAllFilms();
    public function getFilmByID($id);
    public function getAllFilmsInfo();
    public function getCategoryByID($id);
    public function getActorById($id);
}